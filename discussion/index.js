// DIFFERENCE
/*
1. quotation marks
JS Objects only has the value inserted inside quotation marks
JSON has the quotation marks for both key and the value


2.
-JS Objects - exclusive to javascript, other programming languages cannot use object file
-JSON - not exclusive for javascript, other programming languages can also use JSON file
*/

/*
create a JS Object with the following properties
	city
	province
	country

*/

/*let address = {
	city:"Boac",
	province:"Marinduque",
	country:"Philippines",
}
console.log(address);*/


// JSON Object

/*
	Javascript Object Notation
	JSON-used for serializing/deserializing different data types into byte
		Serialization - process of converting data into series of bytes for easier transmission or transfer of information

*/


/*let address = {
	"city":"Boac",
	"province":"Marinduque",
	"country":"Philippines",
};
console.log(address);*/

// JSON Arrays
let cities = [
{
	"city":"Boac",
	"province":"Marinduque",
	"country":"Philippines",
},
{
	"city":"Gasan",
	"province":"Marinduque",
	"country":"Philippines",
},
{
	"city":"Gasan",
	"province":"Marinduque",
	"country":"Philippines",
},
]
console.log(cities);



// JSON METHODS

	// JSON object contains methods for parsing and converting data into stringified JSON
	// Stringified JSON - JSON Object converted into string to be used in other functions of the language esp javascript-based applications(serialize)
let batches = [
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
},

]
console.log("Result from console.log method");
console.log(batches);


console.log("Result from stringify method");
// stringify - used to convert JSON Objects into JSON (string)
console.log(JSON.stringify(batches));



// using stringify for JS Objects
/*let data = JSON.stringify({
	name:"John",
	age:31,
	address:{
		city:"Manila",
		country:"Philippines",
	}

});
console.log(data);*/


/*ASSIGNMENT
	create userDetails variable that will contain js objects with the following properties
		fname - prompt
		lname - prompt
		age - prompt
		address:{
			city - prompt
			country - prompt
			zipcode - prompt

		}
		login the console the converted JSON data type

*/

/*let person = JSON.stringify({
	fname:prompt("Please enter your first name"),
	lname:prompt("Please enter your last name"),
	age:prompt("Please enter your age"),
	address:{
		city:prompt("City"),
		country:prompt("Country"),
		zipcode:prompt("Zipcode"),
	}

});
console.log(person);
*/


// CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	// PARSE Method - converting json data into js objects
	// information is commonly sent to application in STRINGIFIED JSON;then converted into objects; this ha
		// this happens both for sending information to a backend app such as databases and back to frontend app such as the webpages
	// upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method

let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));



let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;

// how do we convert the JSON String above into a JS Object and log it in our console?
console.log(JSON.parse(stringifiedData));